# Social Network for community

## Technologies stack

-   NodeJS
-   MongoDB
-   Express
-   Typescript

## Command remembers

-   Open terminal command windows: Ctrl + `
-   Create file package.json: npm init or yarn init
-   Setup with git:
    -   git init
    -   git add .
    -   git commit -m "Initial commit"
    -   git config user.name "box100101"
    -   git config user.email "duytuong100101@gmail.com"
    -   git remote add origin https://gitlab.com/box100101/social_network_backend.git
    -   git push --set-upstream origin master

## Middleware packages

-   hpp: 
-   helmet: 
-   morgan: config format of logs
    -   winston: 
-   cors:

## Security libraries

-   bcrypt: hash the password

## HTTP Status Code

1. 1xx (100 – 199): Information responses:
    -   Phản hồi thông tin – Yêu cầu đã được chấp nhận và quá trình xử lý yêu cầu của bạn đang được tiếp tục.

2. 2xx (200 – 299): Successful responses:
    -   Phản hồi thành công – Yêu cầu của bạn đã được máy chủ tiếp nhận, hiểu và xử lý thành công.

3. 3xx (300 – 399): Redirects:
    -   Điều hướng – Phía client cần thực hiện hành động bổ sung để hoàn tất yêu cầu.

4. 4xx (400 – 499): Client errors:
    -   Lỗi phía client – Yêu cầu không thể hoàn tất hoặc yêu cầu chứa cú pháp không chính xác. 4xx sẽ hiện ra khi có lỗi từ phía client do không   đưa ra yêu cầu hợp lệ.

5. 5xx (500 – 599): Server errors:
    -   Lỗi phía máy chủ – Máy chủ không thể hoàn thành yêu cầu được cho là hợp lệ. Khi 5xx xảy ra, bạn chỉ có thể đợi để bên hệ thống máy chủ xử lý xong.