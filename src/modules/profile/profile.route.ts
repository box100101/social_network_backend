import { IRoute } from "@core/interfaces";
import { Router } from "express";
import ProfileController from "./profile.controller";
import { authMiddleware } from "@core/middleware";

class ProfileRoute implements IRoute {
  public path = "/api/profile/";
  private pathCreate = "create";
  private pathGetProfile = "get_profile";

  public router = Router();
  private profileController = new ProfileController();

  constructor() {
    this.initializeRoutes();
  }

  public initializeRoutes = () => {
    // POST: url/api/profile/create
    this.router.post(
      this.path + this.pathCreate,
      authMiddleware({ role: "any" }),
      this.profileController.createProfile
    );

    // GET: url/api/profile/get_profile
    this.router.get(
      this.path + this.pathGetProfile,
      authMiddleware({ role: "any" }),
      this.profileController.getProfile
    );
  };
}

export default ProfileRoute;
