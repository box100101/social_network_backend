export class CreateProfileDTO {
  public bio: string;
  public social: {
    youtube?: string;
    facebook?: string;
    instagram?: string;
    twitter?: string;
    linkedin?: string;
  };
  public status: string;
  public website: string;
  public company: string;
  public location: string;
  public marital_status: string;

  constructor(
    bio: string,
    status: string,
    company: string,
    website: string,
    location: string,
    social: {
      youtube?: string;
      facebook?: string;
      instagram?: string;
      twitter?: string;
      linkedin?: string;
    },
    marital_status: string
  ) {
    this.bio = bio;
    this.status = status;
    this.social = social;
    this.website = website;
    this.company = company;
    this.location = location;
    this.marital_status = marital_status;
  }
}
