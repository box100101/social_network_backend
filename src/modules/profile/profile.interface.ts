export interface IMaritalStatus {
  id: number;
  name: string;
}

export interface IProfile {
  user: string;
  company: string;
  website: string;
  skills: string[];
  location: string;
  status: string;
  experience: IExperience[];
  marital_status: string;
  education: IEducation[];
  social: ISocial;
  bio: string;
  follower: IFollower[];
  following: IFollowing[];
}

export interface ISocial extends Record<string, string> {
  youtube: string;
  facebook: string;
  instagram: string;
  twitter: string;
  linkedin: string;
}

export interface IEducation {
  _id: string;
  from: Date;
  to: Date;
  school: string;
  degree: string;
  major: string;
  current: boolean;
}

export interface IExperience {
  _id: string;
  from: Date;
  to: Date;
  title: string;
  company: string;
  location: string;
  current: boolean;
  description: string;
}

export interface IFollower {
  name: string;
  userId: string;
  avatar: string;
  created_date: Date;
}

export interface IFollowing {
  name: string;
  userId: string;
  avatar: string;
  created_date: Date;
}
