import mongoose from "mongoose";
import { IMaritalStatus, IProfile } from "./profile.interface";

const ProfileSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
  },
  company: {
    type: String,
  },
  website: {
    type: String,
  },
  skills: {
    type: [String],
    required: true,
  },
  bio: {
    type: String,
  },
  status: {
    type: String,
    required: true,
  },
  marital_status: {
    type: String,
    default: "0",
  },
  experience: [
    {
      title: {
        type: String,
        required: true,
      },
      company: {
        type: String,
        required: true,
      },
      location: {
        type: String,
      },
      from: {
        type: Date,
        required: true,
      },
      to: {
        type: Date,
      },
      current: {
        type: Boolean,
        default: false,
      },
      description: {
        type: String,
      },
    },
  ],
  education: [
    {
      from: {
        type: Date,
        required: true,
      },
      to: {
        type: Date,
        // required: true,
      },
      school: {
        type: String,
        required: true,
      },
      degree: {
        type: String,
        required: true,
      },
      major: {
        type: String,
        required: true,
      },
      current: {
        type: Boolean,
        default: false,
      },
    },
  ],
  social: {
    youtube: {
      type: String,
    },
    facebook: {
      type: String,
    },
    instagram: {
      type: String,
    },
    twitter: {
      type: String,
    },
    linkedin: {
      type: String,
    },
  },
  followers: [
    {
      user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user",
      },
      created_date: {
        type: Date,
        default: Date.now(),
      },
    },
  ],
  following: [
    {
      user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user",
      },
      created_date: {
        type: Date,
        default: Date.now(),
      },
    },
  ],
});

const MaritalStatusSchema = new mongoose.Schema({
  id: Number,
  name: String,
});

const ProfileModel = mongoose.model<IProfile & mongoose.Document>(
  "profile",
  ProfileSchema
);

const MaritalStatusModel = mongoose.model<IMaritalStatus & mongoose.Document>(
  "marital_status",
  MaritalStatusSchema
);

// EXPORTS ->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
export { MaritalStatusModel };
export default ProfileModel;
