import { NextFunction, Request, Response } from "express";
import ProfileService from "./profile.service";
import { formatJSONResponse } from "@core/utils/helpers";

class ProfileController {
  private profileService = new ProfileService();

  public createProfile = async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    const _id = req._id;
    const data = req.body;

    try {
      await this.profileService.createAndUpdateProfile({
        userId: _id,
        data,
      });

      res.status(201).json(formatJSONResponse(null, "Tạo hồ sơ thành công"));
    } catch (error) {
      next(error);
    }
  };

  public getProfile = async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    const _id = req._id;

    try {
      const profile = await this.profileService.getProfile({
        userId: _id,
      });

      res.status(200).json(formatJSONResponse(profile));
    } catch (error) {
      next(error);
    }
  };
}

export default ProfileController;
