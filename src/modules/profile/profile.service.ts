import { HttpException } from "@core/exceptions";
import { IMaritalStatus, IProfile, ISocial } from "./profile.interface";
import ProfileModel, { MaritalStatusModel } from "./profile.model";
import normalizeUrl from "normalize-url";

class ProfileService {
  private profileModel = ProfileModel;
  private maritalStatusModel = MaritalStatusModel;

  public createAndUpdateProfile = async ({
    userId,
    data,
  }: {
    userId: string;
    data: IProfile;
  }): Promise<void> => {
    const {
      bio,
      skills,
      social,
      status,
      website,
      company,
      location,
      education,
      experience,
      marital_status,
    } = data;

    const baseData: Partial<IProfile> = {
      user: userId,
      bio,
      skills: Array.isArray(skills)
        ? skills
        : (skills as string)
            ?.split(",")
            .map((skill: string) => " " + skill?.trim()),
      status,
      website: website
        ? normalizeUrl(website.toString(), { forceHttps: true })
        : "",
      company,
      location,
      marital_status,
    };

    if (social) {
      const socialField: ISocial = {
        facebook: social.facebook,
        instagram: social.instagram,
        linkedin: social.linkedin,
        twitter: social.twitter,
        youtube: social.youtube,
      };
      for (const [key, value] of Object.entries(socialField)) {
        if (value) {
          socialField[key] = normalizeUrl(value, { forceHttps: true });
        }
      }

      baseData.social = socialField;
    }

    const profile = await this.profileModel
      .findOneAndUpdate(
        {
          user: userId,
        },
        { $set: baseData },
        {
          // new: true,
          upsert: true,
          returnDocument: "after",
          setDefaultsOnInsert: true,
        }
      )
      .exec();

    if (!profile) {
      throw new HttpException(500, "Lỗi khi tạo hồ sơ");
    }
  };

  public getProfile = async ({
    userId,
  }: {
    userId: string;
  }): Promise<
    Omit<IProfile, "marital_status"> & { marital_status: IMaritalStatus | null }
  > => {
    const profile = await this.profileModel
      .findOne({ user: userId })
      .populate("user", "full_name avatar gender role -_id")
      .exec();

    if (!profile) {
      throw new HttpException(404, "Không tìm thấy hồ sơ tương ứng");
    }

    const maritalStatus: IMaritalStatus | null =
      (await this.maritalStatusModel.findOne({
        id: profile.marital_status,
      })) || null;

    const { _id, __v, ...restProfile } = profile.toObject();

    return {
      ...restProfile,
      marital_status: maritalStatus
        ? { id: maritalStatus?.id, name: maritalStatus?.name }
        : null,
    };
  };
}

export default ProfileService;
