import { Router } from "express";
import { IRoute } from "@core/interfaces";
import UserController from "./user.controller";
import { authMiddleware, validationMiddleware } from "@core/middleware";
import { UserDTO } from "./dtos/user.dto";

class UserRoute implements IRoute {
  public path = "/api/user/";
  public pathUpdateProfile = "update_profile";
  public pathGetAll = "get_all";
  public pathDeleteAccount = "delete_account";

  public router = Router();
  private userController = new UserController();

  constructor() {
    this.initializeRoutes();
  }

  public initializeRoutes = () => {
    // GET: url/api/user/get_all
    this.router.get(
      this.path + this.pathGetAll,
      authMiddleware({ role: "admin" }),
      this.userController.getAllUsers
    );

    // PATCH: url/api/user/update_profile
    this.router.patch(
      this.path + this.pathUpdateProfile,
      validationMiddleware(UserDTO, true),
      authMiddleware({ role: "any" }),
      this.userController.updateUserProfile
    );

    // DELETE: url/api/user/delete_account
    this.router.delete(
      this.path + this.pathDeleteAccount,
      authMiddleware({ role: "any" }),
      this.userController.deleteAccount
    );
  };
}

export default UserRoute;
