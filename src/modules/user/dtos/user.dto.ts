import { IsNotEmpty, IsNumber, IsString } from "class-validator";

export class UserDTO {
  @IsString()
  @IsNotEmpty()
  public full_name: string;

  public email: string;

  @IsNumber()
  public gender: number;

  @IsString()
  public avatar: string;

  constructor(
    full_name: string,
    email: string,
    gender: number,
    avatar: string
  ) {
    this.full_name = full_name;
    this.avatar = avatar;
    this.gender = gender;
    this.email = email;
  }
}
