import { HttpException } from "@core/exceptions";
import IUser, { IGender } from "./user.interface";
import UserModel, { GenderModel } from "./user.model";
import { IGetListFunction } from "@core/interfaces";
class UserService {
  private userModel = UserModel;
  private genderSchema = GenderModel;

  public getUserProfile = async (
    _id: string
  ): Promise<Omit<IUser, "gender"> & { gender: IGender | null }> => {
    const user: IUser | null = await this.userModel.findById(_id);

    if (!user) {
      throw new HttpException(404, "Không tìm thấy user tương ứng");
    }

    const gender: IGender | null = await this.genderSchema.findOne({
      id: user.gender,
    });

    return {
      birth_date: user.birth_date,
      email: user.email,
      full_name: user.full_name,
      gender: gender ? { id: gender?.id, name: gender?.name } : null,
      avatar: user.avatar,
      role: user.role,
    };
  };

  public updateUserProfile = async ({
    _id,
    updatedData,
  }: {
    _id: string;
    updatedData: IUser;
  }): Promise<Omit<IUser, "gender"> & { gender: IGender | null }> => {
    if (updatedData.email) {
      throw new HttpException(400, "Không thể cập nhật trường email");
    }

    const user = await this.userModel.findByIdAndUpdate(_id, updatedData, {
      returnDocument: "after",
    });

    if (!user) {
      throw new HttpException(404, "Không tìm thấy user tương ứng");
    }

    const gender: IGender | null = await this.genderSchema.findOne({
      id: user.gender,
    });

    return {
      birth_date: user.birth_date,
      email: user.email,
      full_name: user.full_name,
      gender: gender ? { id: gender?.id, name: gender?.name } : null,
      avatar: user.avatar,
      role: user.role,
    };
  };

  public getAllUsers = async (
    page: string,
    pageSize: string
  ): Promise<IGetListFunction<IUser>> => {
    const _page = parseInt(page) > 0 ? parseInt(page) : 0;
    const _pageSize = parseInt(pageSize) > 0 ? parseInt(pageSize) : 10;
    const allUsers: IUser[] = await this.userModel
      .find()
      .skip(_page * _pageSize)
      .limit(_pageSize)
      .exec();

    const totalUsers: number = await this.userModel.countDocuments();

    return {
      list: allUsers,
      totalItem: totalUsers,
      page: _page || 0,
      pageSize: _pageSize,
    };
  };

  public deleteAccount = async (_id: string): Promise<void> => {
    const user = await this.userModel.findByIdAndDelete(_id);

    if (!user) {
      throw new HttpException(404, "Không tìm thấy user tương ứng");
    }
  };
}

export default UserService;
