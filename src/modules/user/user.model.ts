import mongoose from "mongoose";
import IUser, { IGender } from "./user.interface";

const User = new mongoose.Schema({
  full_name: {
    type: String,
    required: true,
  },
  birth_date: {
    type: Date,
    default: Date.now(),
  },
  gender: {
    type: Number,
  },
  email: {
    type: String,
    unique: true,
    index: true,
    required: true,
    immutable: true,
  },
  password: {
    type: String,
    required: true,
  },
  avatar: {
    type: String,
  },
  role: {
    type: String,
  },
});

const UserModel = mongoose.model<IUser & mongoose.Document>("user", User);

// Gender ->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
const Gender = new mongoose.Schema({
  id: Number,
  name: String,
});

const GenderModel = mongoose.model<IGender & mongoose.Document>(
  "gender",
  Gender
);

// EXPORTS ->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
export { GenderModel };
export default UserModel;
