import { NextFunction, Request, Response } from "express";
import UserService from "./user.service";
import {
  formatJSONListResponse,
  formatJSONResponse,
} from "@core/utils/helpers";

class UserController {
  private userService = new UserService();

  public getUserProfile = async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    const _id = req._id;

    try {
      const userProfile = await this.userService.getUserProfile(_id);

      res.status(200).json(formatJSONResponse(userProfile));
    } catch (error) {
      next(error);
    }
  };

  public updateUserProfile = async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    const _id = req._id;
    const updatedData = req.body;

    try {
      const userProfile = await this.userService.updateUserProfile({
        _id,
        updatedData,
      });

      res.status(200).json(formatJSONResponse(userProfile));
    } catch (error) {
      next(error);
    }
  };

  public getAllUsers = async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    const page: any = req.query.page;
    const pageSize: any = req.query.page_size;

    try {
      const data = await this.userService.getAllUsers(page, pageSize);

      res.status(200).json(
        formatJSONListResponse({
          list: data.list,
          page: data.page,
          pageSize: data.pageSize,
          totalItem: data.totalItem,
        })
      );
    } catch (error) {
      next(error);
    }
  };

  public deleteAccount = async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    const _id = req._id;

    try {
      await this.userService.deleteAccount(_id);

      res
        .status(200)
        .json(formatJSONResponse(null, "Xoá tài khoản thành công"));
    } catch (error) {
      next(error);
    }
  };
}

export default UserController;
