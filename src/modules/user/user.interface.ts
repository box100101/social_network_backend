export interface IGender {
  id: number;
  name: string;
}

interface IUser {
  _id?: string;
  full_name: string;
  birth_date: Date;
  email: string;
  password?: string;
  avatar?: string;
  gender: number;
  role: "admin" | "user";
}

export default IUser;
