import { IsEmail, IsNotEmpty, MinLength } from "class-validator";

export class SignInDTO {
  @IsNotEmpty({ message: "Email chưa có giá trị" })
  public email: string;

  @IsNotEmpty({ message: "Password chưa có giá trị" })
  public password: string;

  constructor(password: string, email: string) {
    this.password = password;
    this.email = email;
  }
}

export class SignUpDTO {
  @IsNotEmpty()
  public full_name: string;

  public avatar: string;

  @IsEmail({}, { message: "Email không đúng định dạng" })
  public email: string;

  @IsNotEmpty()
  @MinLength(6)
  public password: string;

  constructor(
    full_name: string,
    avatar: string,
    password: string,
    email: string
  ) {
    this.full_name = full_name;
    this.avatar = avatar;
    this.password = password;
    this.email = email;
  }
}

export class ChangePassDTO {
  @IsNotEmpty({ message: "Mật khẩu cũ chưa có giá trị" })
  public old_password: string;

  @MinLength(6, { message: "Mật khẩu mới tối thiểu 6 kí tự" })
  public new_password: string;

  constructor(old_password: string, new_password: string) {
    this.old_password = old_password;
    this.new_password = new_password;
  }
}
