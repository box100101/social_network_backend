import { IUserData } from "./auth.interface";
import AuthRoute from "./auth.route";

export { IUserData, AuthRoute };
