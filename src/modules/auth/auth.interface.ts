export interface IUserData {
  token: string;
  avatar: string | null;
  email: string;
  role: "admin" | "user";
}
