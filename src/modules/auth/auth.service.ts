import { IUserData } from "@modules/auth";
import { HttpException } from "@core/exceptions";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import IUser from "@modules/user/user.interface";
import { ChangePassDTO, SignInDTO, SignUpDTO } from "./auth.dto";
import UserModel from "@modules/user/user.model";

class AuthService {
  private userModel = UserModel;

  public async signIn(reqData: SignInDTO): Promise<IUserData> {
    const user: IUser | null = await this.userModel.findOne({
      email: reqData.email,
    });

    if (!user) {
      throw new HttpException(409, "Email không tồn tại");
    }

    const isMatchPassword: boolean = await bcrypt.compare(
      reqData.password,
      user.password!
    );

    if (!isMatchPassword) {
      throw new HttpException(401, "Password không chính xác!");
    }

    return {
      avatar: user.avatar || null,
      email: user.email,
      token: this.createToken(user._id!, user.role),
      role: user.role,
    };
  }

  public async signUp(
    reqData: SignUpDTO,
    role: "admin" | "user" = "user"
  ): Promise<IUserData> {
    const user = await this.userModel.findOne({ email: reqData.email });

    if (user) {
      throw new HttpException(409, "Email đăng ký đã tồn tại!");
    }

    // hash password by bcrypt
    const saltRounds = 10;
    const hashPassword = await bcrypt.hash(reqData.password!, saltRounds);

    const createdUser: IUser = await this.userModel.create({
      ...reqData,
      avatar: reqData.avatar || null,
      password: hashPassword,
      birth_date: Date.now(),
      role: role,
    });

    return {
      avatar: createdUser.avatar || null,
      email: createdUser.email,
      token: this.createToken(createdUser._id!, role),
      role: createdUser.role,
    };
  }

  public async changePassword(reqData: {
    passwordData: ChangePassDTO;
    _id: string;
  }): Promise<void> {
    const user: IUser | null = await this.userModel.findById(reqData._id);

    if (!user) {
      throw new HttpException(404, "Không tìm thấy user tương ứng");
    }

    const isMatch = await bcrypt.compare(
      reqData.passwordData.old_password,
      user.password!
    );

    if (!isMatch) {
      throw new HttpException(401, "Mật khẩu cũ không chính xác");
    }

    // hash password by bcrypt
    const saltRounds = 10;
    const hashPassword = await bcrypt.hash(
      reqData.passwordData.new_password!,
      saltRounds
    );

    await this.userModel.findByIdAndUpdate(
      reqData._id,
      {
        password: hashPassword,
      },
      { returnDocument: "after" }
    );
  }

  private createToken(_id: string, role: string): string {
    const payload: { id: string; role: string } = { id: _id, role: role };
    const secretKey: string = process.env.JWT_TOKEN_SECRET_KEY!;
    const expiresIn: number = 604800;

    const token: string = jwt.sign(payload, secretKey, {
      expiresIn,
    });

    return token;
  }
}

export default AuthService;
