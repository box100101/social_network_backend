import { Router } from "express";
import { IRoute } from "@core/interfaces";
import { authMiddleware, validationMiddleware } from "@core/middleware";
import AuthController from "./auth.controller";
import { ChangePassDTO, SignInDTO, SignUpDTO } from "./auth.dto";

class AuthRoute implements IRoute {
  public path = "/api/auth/";
  public pathSignIn = "sign_in";
  public pathSignUp = "sign_up";
  public pathSignUpAdmin = "sign_up_admin";
  public pathChangePass = "change_pass";

  public router = Router();
  private authController = new AuthController();

  constructor() {
    this.initializeRoutes();
  }

  public initializeRoutes = () => {
    // GET: url/api/auth/sign_in
    this.router.get(
      this.path + this.pathSignIn,
      validationMiddleware(SignInDTO, false, "params"),
      this.authController.signIn
    );

    // POST: url/api/auth/sign_up
    this.router.post(
      this.path + this.pathSignUp,
      validationMiddleware(SignUpDTO, false),
      this.authController.signUp
    );

    // POST: url/api/auth/sign_up_admin
    this.router.post(
      this.path + this.pathSignUpAdmin,
      validationMiddleware(SignUpDTO, false),
      this.authController.signUpAdmin
    );

    // POST: url/api/auth/change_pass
    this.router.post(
      this.path + this.pathChangePass,
      authMiddleware({ role: "any" }),
      validationMiddleware(ChangePassDTO, false),
      this.authController.changePassword
    );
  };
}

export default AuthRoute;
