import { NextFunction, Request, Response } from "express";
import AuthService from "./auth.service";
import { IUserData } from "./auth.interface";
import { formatJSONResponse } from "@core/utils/helpers";
import { ChangePassDTO, SignInDTO, SignUpDTO } from "./auth.dto";

class AuthController {
  private authService = new AuthService();

  public signIn = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const params: SignInDTO = {
        email: (req.query?.email as string) || "",
        password: (req.query?.password as string) || "",
      };

      const userData: IUserData = await this.authService.signIn(params);

      res.status(200).json(formatJSONResponse(userData));
    } catch (error) {
      next(error);
    }
  };

  public signUp = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const reqBody: SignUpDTO = req.body;

      await this.authService.signUp(reqBody);

      res
        .status(201)
        .json(formatJSONResponse(null, "Tạo tài khoản thành công"));
    } catch (error) {
      next(error);
    }
  };

  public signUpAdmin = async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    try {
      const reqBody: SignUpDTO = req.body;

      await this.authService.signUp(reqBody, "admin");

      res
        .status(201)
        .json(formatJSONResponse(null, "Tạo tài khoản thành công"));
    } catch (error) {
      next(error);
    }
  };

  public changePassword = async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    const _id = req._id;
    const passwordData: ChangePassDTO = req.body;

    try {
      await this.authService.changePassword({
        _id,
        passwordData,
      });

      res
        .status(200)
        .json(formatJSONResponse(null, "Thay đổi mật khẩu thành công"));
    } catch (error) {
      next(error);
    }
  };
}

export default AuthController;
