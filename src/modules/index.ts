import { AuthRoute } from "./auth";
import { UserRoute } from "./user";
import { ProfileRoute } from "./profile";
import { PostsRoute } from "./posts";

export { AuthRoute, ProfileRoute, UserRoute, PostsRoute };
