import { UserModel } from "@modules/user";
import { CreatePostsDTO } from "./dtos";
import PostsModel from "./posts.model";
import { HttpException } from "@core/exceptions";
import {
  IComment,
  IPosts,
  IReactionOfUser,
  ReactionTypes,
} from "./posts.interface";
import { IGetListFunction } from "@core/interfaces";
import mongoose from "mongoose";

class PostsService {
  private postsModel = PostsModel;
  private userModel = UserModel;

  public createPosts = async ({
    reqData,
    userId,
  }: {
    userId: string;
    reqData: CreatePostsDTO;
  }): Promise<IPosts> => {
    const user = await this.userModel.findById(userId);

    if (!user) {
      throw new HttpException(404, "Người dùng không tồn tại!");
    }

    const createdPosts = await this.postsModel.create({
      content: reqData.content || "",
      avatar: user.avatar,
      user: userId,
      name_of_user: user.full_name,
      image: reqData.image || "",
    });

    return createdPosts;
  };

  public editPosts = async ({
    reqData,
    postsId,
  }: {
    postsId: string;
    reqData: CreatePostsDTO;
  }): Promise<IPosts> => {
    if (!postsId) {
      throw new HttpException(400, "ID bài viết không tồn tại!");
    }

    let editPosts;

    try {
      editPosts = await this.postsModel.findByIdAndUpdate(postsId, reqData, {
        new: true,
      });
    } catch (error) {
      throw new HttpException(404, "ID bài viết không hợp lệ!");
    }

    if (!editPosts) {
      throw new HttpException(404, "Không tìm thấy bài viết tương ứng!");
    }

    return editPosts;
  };

  public getAllPosts = async (
    page: string = "0",
    pageSize: string = "10"
  ): Promise<IGetListFunction<IPosts>> => {
    const _page = parseInt(page);
    const _pageSize = parseInt(pageSize);

    const allPosts: IPosts[] = await this.postsModel
      .find()
      .skip(_page * _pageSize)
      .limit(_pageSize)
      .exec();

    const totalPosts = await this.postsModel.countDocuments();

    return {
      page: _page,
      list: allPosts,
      pageSize: _pageSize,
      totalItem: totalPosts,
    };
  };

  public deletePostsById = async ({
    idPosts,
    idUser,
    role,
  }: {
    idPosts: string;
    idUser: string;
    role: "user" | "admin";
  }): Promise<void> => {
    if (!idPosts) {
      throw new HttpException(400, "ID bài viết không tồn tại!");
    }

    if (!mongoose.Types.ObjectId.isValid(idPosts.toString())) {
      throw new HttpException(400, "ID bài viết không hợp lệ!");
    }

    const posts = await this.postsModel.findById(idPosts).exec();

    if (!posts) {
      throw new HttpException(404, "Không tìm thấy bài viết tương ứng!");
    }

    if (posts?.user !== idUser && role !== "admin") {
      throw new HttpException(403, "Bạn không có quyền xóa bài viết này!");
    }

    try {
      await posts.deleteOne();
    } catch (error) {
      throw new HttpException(501, "Không thể xoá được bài viết!");
    }
  };

  public reactPosts = async ({
    idPosts,
    idUser,
    typeOfReaction,
  }: {
    idPosts: string;
    idUser: string;
    typeOfReaction: ReactionTypes;
  }): Promise<IPosts> => {
    if (!mongoose.Types.ObjectId.isValid(idPosts)) {
      throw new HttpException(400, "ID bài viết không hợp lệ!");
    }

    const posts = await this.postsModel.findById(idPosts);

    if (!posts) {
      throw new HttpException(404, "Không tìm thấy bài viết tương ứng!");
    }

    switch (typeOfReaction) {
      case "like":
        if (
          posts.likes.some(
            (like: IReactionOfUser) => like.user.toString() === idUser
          )
        ) {
          posts.likes = posts.likes.filter(
            (like: IReactionOfUser) => like.user.toString() !== idUser
          );
        } else {
          posts.likes.unshift({ user: idUser });

          posts.dislikes = posts.dislikes.filter(
            (like: IReactionOfUser) => like.user.toString() !== idUser
          );
        }
        posts.save();
        break;
      case "dislike":
        if (
          posts.dislikes.some(
            (like: IReactionOfUser) => like.user.toString() === idUser
          )
        ) {
          posts.dislikes = posts.dislikes.filter(
            (like: IReactionOfUser) => like.user !== idUser
          );
        } else {
          posts.dislikes.unshift({ user: idUser });

          posts.likes = posts.likes.filter(
            (like: IReactionOfUser) => like.user.toString() !== idUser
          );
        }
        posts.save();
        break;
    }

    return posts;
  };

  public addComment = async ({
    idPosts,
    content,
    idUser,
  }: {
    idUser: string;
    content: string;
    idPosts: string;
  }): Promise<IComment[]> => {
    const user = await this.userModel.findById(idUser);
    const posts = await this.postsModel.findById(idPosts);

    if (!posts) {
      throw new HttpException(404, "Không tìm thấy bài viết tương ứng!");
    }

    if (!user) throw new HttpException(404, "Không tìm thấy user tương ứng!");

    const addedComment: IComment = {
      user: idUser,
      content: content,
      name: user.full_name,
      avatar: user.avatar || "",
    };

    posts.comments.unshift(addedComment);
    await posts.save();

    return posts.comments;
  };

  public deleteComment = async ({
    idPosts,
    idUser,
    idComment,
  }: {
    idUser: string;
    idPosts: string;
    idComment: string;
  }): Promise<void> => {
    if (
      !mongoose.Types.ObjectId.isValid(idPosts) ||
      !mongoose.Types.ObjectId.isValid(idComment)
    ) {
      throw new HttpException(
        400,
        "ID bài viết hoặc ID bình luận không hợp lệ!"
      );
    }

    const posts = await this.postsModel.findById(idPosts);

    if (!posts) {
      throw new HttpException(404, "Không tìm thấy bài viết tương ứng!");
    }

    const comment = posts.comments.find(
      (cmt) => cmt._id?.toString() === idComment
    );
    if (!comment)
      throw new HttpException(404, "Không tìm thấy comment tương ứng!");

    if (idUser !== posts.user && comment.user?.toString() !== idUser)
      throw new HttpException(403, "Bạn không có quyền xóa comment này!");

    posts.comments = posts.comments.filter(
      (comment: IComment) => comment._id?.toString() !== idComment
    );

    await posts.save();
  };
}

export default PostsService;
