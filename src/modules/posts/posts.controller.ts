import { Response, NextFunction, Request } from "express";
import PostsService from "./posts.service";
import {
  formatJSONListResponse,
  formatJSONResponse,
} from "@core/utils/helpers";

class PostsController {
  private postsService = new PostsService();

  public createPosts = async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    const _id = req._id;
    const reqData = req.body;

    try {
      const createdPosts = await this.postsService.createPosts({
        userId: _id,
        reqData: reqData,
      });

      res.status(201).json(formatJSONResponse(createdPosts));
    } catch (error) {
      next(error);
    }
  };

  public editPosts = async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    // @params api: {id: string}
    const postsId = req.body?.id;
    const reqData = req.body;

    try {
      const editPosts = await this.postsService.editPosts({ postsId, reqData });

      res.status(200).json(formatJSONResponse(editPosts));
    } catch (error) {
      next(error);
    }
  };

  public getAllPosts = async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    const query = {
      page: req.params?.page,
      page_size: req.params?.pageSize,
    };

    try {
      const data = await this.postsService.getAllPosts(
        query.page,
        query.page_size
      );

      res.status(200).json(
        formatJSONListResponse({
          list: data.list,
          page: data.page,
          pageSize: data.pageSize,
          totalItem: data.totalItem,
        })
      );
    } catch (error) {
      next(error);
    }
  };

  public deletePostsById = async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    const idPosts = req.body.id;
    const idUser = req._id;
    const roleUser = req.userRole;

    try {
      await this.postsService.deletePostsById({
        idPosts,
        idUser,
        role: roleUser,
      });

      res.status(200).json(formatJSONResponse("Đã xoá bài viết thành công"));
    } catch (error) {
      next(error);
    }
  };

  public reactPosts = async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    const idUser = req._id;
    const idPosts = req.body.id;
    const typeOfReaction = req.body.type_of_reaction;

    try {
      const posts = await this.postsService.reactPosts({
        idPosts,
        idUser,
        typeOfReaction,
      });

      res.status(200).json(formatJSONResponse(posts));
    } catch (error) {
      next(error);
    }
  };

  public addComment = async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    const idUser = req._id;
    const idPosts = req.body.id_posts;
    const content = req.body.content;

    try {
      const comment = await this.postsService.addComment({
        idPosts,
        idUser,
        content,
      });

      res.status(201).json(formatJSONResponse(comment));
    } catch (error) {
      next(error);
    }
  };

  public deleteComment = async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    const idUser = req._id;
    const idPosts = req.body.id_posts;
    const idComment = req.body.id_comment;

    try {
      await this.postsService.deleteComment({
        idComment,
        idUser,
        idPosts,
      });

      res.status(200).json(formatJSONResponse("Đã xoá bình luận thành công"));
    } catch (error) {
      next(error);
    }
  };
}

export default PostsController;
