import { authMiddleware, validationMiddleware } from "@core/middleware";
import { AddCommentDTO, CreatePostsDTO, ReactPostsDTO } from "./dtos";
import PostsController from "./posts.controller";
import { Router } from "express";
import { IRoute } from "@core/interfaces";
import { DeleteCommentDTO } from "./dtos/comment_posts.dto";

class PostsRoute implements IRoute {
  public path = "/api/posts/";
  private createPath = "create";
  private editPath = "edit";
  private getAllPath = "get_all";
  private deleteByIdPath = "delete_by_id";
  private reactPostsPath = "react_posts";
  private addCommentPath = "add_comment";
  private deleteCommentPath = "delete_comment";

  public router = Router();
  private postsController = new PostsController();

  constructor() {
    this.initializeRoutes();
  }

  public initializeRoutes = () => {
    // POST: url/api/posts/create
    this.router.post(
      this.path + this.createPath,
      authMiddleware({ role: "any" }),
      validationMiddleware(CreatePostsDTO, false),
      this.postsController.createPosts
    );
    // PUT: url/api/posts/edit
    this.router.put(
      this.path + this.editPath,
      authMiddleware({ role: "any" }),
      validationMiddleware(CreatePostsDTO, true),
      this.postsController.editPosts
    );
    // GET: url/api/posts/all?page=1&page_size=10
    this.router.get(
      this.path + this.getAllPath,
      authMiddleware({ role: "any" }),
      this.postsController.getAllPosts
    );
    // DELETE: url/api/posts/delete_by_id
    this.router.delete(
      this.path + this.deleteByIdPath,
      authMiddleware({ role: "any" }),
      this.postsController.deletePostsById
    );
    // PUT: url/api/posts/react_posts?id=1&type_of_reaction=like
    this.router.put(
      this.path + this.reactPostsPath,
      authMiddleware({ role: "any" }),
      validationMiddleware(ReactPostsDTO),
      this.postsController.reactPosts
    );

    // POST: url/api/posts/add_comment
    /**
     * params:
     * id_posts, content
     */
    this.router.post(
      this.path + this.addCommentPath,
      authMiddleware({ role: "any" }),
      validationMiddleware(AddCommentDTO, false, "body"),
      this.postsController.addComment
    );

    // DELETE: url/api/posts/delete_comment
    /**
     * params:
     * id_posts, id_comment
     */
    this.router.delete(
      this.path + this.deleteCommentPath,
      authMiddleware({ role: "any" }),
      validationMiddleware(DeleteCommentDTO, false, "body"),
      this.postsController.deleteComment
    );
  };
}

export default PostsRoute;
