import mongoose from "mongoose";
import { IPosts } from "./posts.interface";

const PostsSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
  avatar: String,
  name_of_user: String,
  image: String,
  content: {
    type: String,
    required: true,
  },
  likes: [{ user: mongoose.Schema.Types.ObjectId }],
  dislikes: [{ user: mongoose.Schema.Types.ObjectId }],
  comments: [
    {
      user: mongoose.Schema.Types.ObjectId,
      name: String,
      content: {
        type: String,
        required: true,
      },
      avatar: String,
      created_date: {
        type: Date,
        default: Date.now(),
      },
    },
  ],
});

const PostsModel = mongoose.model<IPosts & mongoose.Document>(
  "post",
  PostsSchema
);

export default PostsModel;
