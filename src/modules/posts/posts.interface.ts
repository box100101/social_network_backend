export interface IPosts {
  _id: string;
  created_date: Date;
  user: string;
  name_of_user: string;
  avatar: string;
  image: string;
  content: string;
  likes: IReactionOfUser[];
  dislikes: IReactionOfUser[];
  comments: IComment[];
}

export interface IReactionOfUser {
  user: string;
}

export interface IComment {
  _id?: string;
  user: string;
  name: string;
  content: string;
  avatar: string;
  created_date?: Date;
}

export type ReactionTypes = "like" | "dislike";
