import { IsIn, IsNotEmpty, IsString } from "class-validator";
import { ReactionTypes } from "../posts.interface";

class ReactPostsDTO {
  @IsString()
  @IsNotEmpty({ message: "Thiếu ID của bài viết!" })
  public id: string;

  @IsNotEmpty()
  @IsIn(["like", "dislike"], {
    message: "Loại phản hồi không hợp lệ!",
  })
  public type_of_reaction: ReactionTypes;

  constructor(idPosts: string, typeOfReaction: ReactionTypes) {
    this.id = idPosts;
    this.type_of_reaction = typeOfReaction;
  }
}

export default ReactPostsDTO;
