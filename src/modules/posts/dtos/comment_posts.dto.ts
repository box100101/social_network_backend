import { IsNotEmpty, IsString } from "class-validator";

class AddCommentDTO {
  @IsString({ message: "id bài viết phải thuộc kiểu string!" })
  id_posts: string;

  @IsString({ message: "id bài viết phải thuộc kiểu string!" })
  @IsNotEmpty({ message: "content bài viết không được bỏ trống!" })
  content: string;

  constructor(content: string, id_posts: string) {
    this.id_posts = id_posts;
    this.content = content;
  }
}

class DeleteCommentDTO {
  @IsNotEmpty({ message: "id_posts không được bỏ trống!" })
  id_posts: string;

  @IsNotEmpty({ message: "id_comment không được bỏ trống!" })
  id_comment: string;

  constructor(id_posts: string, id_comment: string) {
    this.id_posts = id_posts;
    this.id_comment = id_comment;
  }
}

export { AddCommentDTO, DeleteCommentDTO };
