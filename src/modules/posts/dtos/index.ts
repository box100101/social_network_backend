import CreatePostsDTO from "./create_posts.dto";
import ReactPostsDTO from "./react_posts.dto";
import { AddCommentDTO } from "./comment_posts.dto";

export { CreatePostsDTO, ReactPostsDTO, AddCommentDTO };
