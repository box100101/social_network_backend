import { IsString } from "class-validator";

class CreatePostsDTO {
  @IsString()
  public image: string;
  @IsString()
  public content: string;

  constructor(image: string, content: string) {
    this.image = image;
    this.content = content;
  }
}

export default CreatePostsDTO;
