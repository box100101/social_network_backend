import { AuthRoute, PostsRoute, ProfileRoute, UserRoute } from "@modules";
import "dotenv/config";
import "reflect-metadata";
import App from "./app";
import { validateEnv } from "@core/utils";

validateEnv();

const routes = [
  new UserRoute(),
  new AuthRoute(),
  new ProfileRoute(),
  new PostsRoute(),
];

const app = new App(routes);

app.listening();
