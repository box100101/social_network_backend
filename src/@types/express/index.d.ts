declare namespace Express {
  interface Request {
    _id: string;
    userRole: "admin" | "user";
  }
}
