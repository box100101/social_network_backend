import express from "express";
import mongoose from "mongoose";
import hpp from "hpp";
import helmet from "helmet";
import cors from "cors";
import morgan from "morgan";
import { IRoute } from "@core/interfaces";
import { Logger } from "@core/utils";
import { errorMiddleware } from "@core/middleware";

class App {
  private app: express.Application;
  private port: string | number;
  private isProductionEnvironment: boolean;

  public listening = (): void => {
    this.app.listen(this.port, () => {
      Logger.info(`The app is listening on port ${this.port}`);
    });
  };

  constructor(routes: IRoute[]) {
    this.app = express();
    this.port = process.env.PORT || 3000;
    this.isProductionEnvironment = process.env.NODE_ENV === "production";

    this.connectDatabase();
    this.initializeMiddlewares();
    this.initializeRoutes(routes);
    this.intializeErrorMiddleware();
  }

  private initializeRoutes = (routes: IRoute[]) => {
    routes.forEach((route) => {
      this.app.use(route.router);
    });
  };

  private intializeErrorMiddleware = () => {
    this.app.use(errorMiddleware);
  };

  private initializeMiddlewares = () => {
    if (this.isProductionEnvironment) {
      this.app.use(hpp());
      this.app.use(helmet());
      this.app.use(morgan("combined"));
      this.app.use(cors({ origin: "your.domain.com", credentials: true }));
    } else {
      this.app.use(morgan("dev"));
      this.app.use(cors({ origin: true, credentials: true }));
    }
    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: true }));
  };

  private connectDatabase = () => {
    if (process.env.CONNECTION_STRING) {
      mongoose
        .connect(process.env.CONNECTION_STRING)
        .then(() => {
          Logger.info("Connected!");
        })
        .catch((reason) => {
          Logger.error("Failed to connect!", reason);
        });
    } else {
      Logger.error("Connection string is not set!");
    }
  };
}

export default App;
