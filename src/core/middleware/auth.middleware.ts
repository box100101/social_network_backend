import { HttpException } from "@core/exceptions";
import { NextFunction, Request, Response } from "express";
import jwt, { JwtPayload, JsonWebTokenError } from "jsonwebtoken";

const authMiddleware = ({
  role = "any",
}: {
  role: "admin" | "user" | "any";
}) => {
  return (req: Request, res: Response, next: NextFunction) => {
    let token: string = req.header("Authorization") || "";
    token = token.replace(/^Bearer\s+/, "");

    if (!token) {
      next(new HttpException(401, "Api yêu cầu kèm token trong header"));
    }

    try {
      const _jwt = jwt.verify(
        token,
        process.env.JWT_TOKEN_SECRET_KEY!
      ) as JwtPayload;

      if (!_jwt.id || !_jwt.role) {
        next(new HttpException(401, "Token không hợp lệ"));
      }

      req._id = _jwt.id;
      req.userRole = _jwt.role;

      if (role !== "any") {
        const _role = _jwt?.role;

        if (_role !== role) {
          next(new HttpException(403, "Bạn không có quyền truy cập"));
        }
      }

      next();
    } catch (error) {
      const err = error as JsonWebTokenError;

      switch (err.name) {
        case "TokenExpiredError":
          next(new HttpException(401, "Token hết hạn"));
          break;
        case "SyntaxError":
        case "JsonWebTokenError":
          next(new HttpException(401, "Token không hợp lệ"));
          break;
        default:
          next(new HttpException(500, "Không xác định được lỗi"));
      }
    }
  };
};

export default authMiddleware;
