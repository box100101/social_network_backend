export const isEmptyObject = (obj: object): boolean => {
  return !Object.keys(obj).length;
};

export const convertDateTime = (dateTime: string): string => {
  return "";
};

export const formatJSONResponse = (
  response?: any,
  message?: string,
  ...extraResponse: any
): any => {
  return {
    data: response || null,
    message: message || "Thành công",
    status: 1,
    ...extraResponse,
  };
};

export const formatJSONListResponse = ({
  list = [],
  page,
  pageSize,
  totalItem,
  message,
}: {
  list: any[];
  pageSize: number;
  page: number;
  totalItem: number;
  message?: string;
}): any => {
  return {
    data: {
      list: list.length > 0 ? list : [],
      page_size: pageSize,
      page: page,
      total_item: totalItem,
    },
    message: message || "Thành công",
    status: 1,
  };
};
