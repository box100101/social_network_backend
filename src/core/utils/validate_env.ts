import { cleanEnv, str } from "envalid";

const validateEnv = () => {
  cleanEnv(process.env, {
    // NODE_ENV: str(),
    CONNECTION_STRING: str(),
  });
};

export default validateEnv;
