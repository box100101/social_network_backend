import IRoute from "./route.interface";
import { IGetListFunction } from "./function.inferface";

export { IRoute, IGetListFunction };
