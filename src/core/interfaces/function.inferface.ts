export interface IGetListFunction<T> {
  list: T[];
  totalItem: number;
  page: number;
  pageSize: number;
}
