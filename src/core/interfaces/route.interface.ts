import { Router } from "express";

interface IRoute {
  path: string;
  router: Router;
  initializeRoutes(): void;
}

export default IRoute;
